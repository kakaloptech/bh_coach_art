var questionState = 1;
var isAnswering = false;

$(document).ready(function() {

    TweenMax.set(".people", { x: -1500, alpha: 0 });
    TweenMax.set(".yellow-bar", { left: "-50%", alpha: 0 });
    TweenMax.set(".title", { right: "200px", alpha: 0 });
    TweenMax.set(".style-box", { left: "-=100px", alpha: 0 });
    TweenMax.set(".yellow-circle", { top: 260, alpha: 0 });
    TweenMax.set(".top", { alpha: 0 });

    $('body').jpreLoader({ splashID: "#jSplash", loaderVPos: '50%', autoClose: true }, function() {
        // preload completed
        TweenMax.to(".people", 1.2, { x: 0, alpha: 1, ease: Expo.easeOut });
        TweenMax.to(".yellow-bar", 1, { left: "50%", alpha: 1, ease: Expo.easeOut });
        TweenMax.to(".title", 1.2, { right: "90px", alpha: 1, ease: Expo.easeOut, delay: .5 });
        TweenMax.to(".bike-a", 1, { left: "+=100px", alpha: 1, ease: Expo.easeOut, delay: .6 });
        TweenMax.to(".bike-b", 1, { left: "+=100px", alpha: 1, ease: Expo.easeOut, delay: .7 });
        TweenMax.to(".bike-c", 1, { left: "+=100px", alpha: 1, ease: Expo.easeOut, delay: .8 });
        TweenMax.to(".yellow-circle", 0.5, { top: 270, alpha: 1, ease: Expo.easeOut, delay: 1 });
        TweenMax.to(".top", 1.5, { alpha: 1, ease: Expo.easeOut, delay: .5 });
    });


    // Test Go
    $(".yellow-circle").on('click', function() {

        if (isAnswering) return false;
        isAnswering = true;
        $(".3style").addClass("hide");
        $(".question").removeClass("hide");
        $(".question1").removeClass("hide");
        $(".yellow-circle").addClass("q1");
    });

    // 選擇答案(加黃色)

    $(".answer-box").on('click', function() {
        $(".answer-box").removeClass("chosen");
        $(this).addClass("chosen");
    });


    // NEXT(跳下一題/跳結果)

    $(".next").on('click', function() {

        if (questionState > 2) {
            $(".question").addClass("hide");
            $(".question" + questionState).addClass("hide");
            $(".yellow-circle").addClass("end").removeClass("q3");
            $(".result").removeClass("hide");
            $(".result1").removeClass("hide");

            TweenMax.from(".result-pic", 1, { right: 500, alpha: 0, ease: Expo.easeOut });
        } else {
            $(".question" + questionState).addClass("hide");

            var nextQuestion = questionState + 1;

            $(".question" + nextQuestion).removeClass("hide");
            $(".yellow-circle").removeClass("q" + questionState).addClass("q" + nextQuestion);

            questionState = nextQuestion;
        }
    });

    // 再測一次

    $(".back").on('click', function() {
        $(".result").addClass("hide");
        $(".3style").removeClass("hide");
        $(".yellow-circle").removeClass("end");

        questionState = 1;
        isAnswering = false;
    });

    // 分享填資料

    $(".share").on('click', function() {
        $(".fillIn").removeClass("hide");
        $(".popup-box").removeClass("hide");

        TweenMax.from(".fillIn .popup-box", .5, { top: "20%", alpha: 0, ease: Power2.easeOut })
    });

    // 送出成功

    $(".send").on('click', function() {
        $(".fillIn .popup-box").addClass("hide");
        $(".success").removeClass("hide");
    });

    // 關閉視窗

    $(".close").on('click', function() {
        $(".fillIn").addClass("hide");
        $(".information").addClass("hide");
        $(".success").addClass("hide");
    });

     // 活動辦法    

    $(".info-btn").on('click', function() {
        $(".information").removeClass("hide");
        $('.scroll-pane').jScrollPane({
            verticalDragMinHeight: 35,
            verticalDragMaxHeight: 35,
            autoReinitialise: true
        });
        TweenMax.from(".information .popup-box", .5, { top: "20%", alpha: 0, ease: Power2.easeOut })
    });

});
